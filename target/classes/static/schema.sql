CREATE TABLE IF NOT EXISTS users
(
      id LONG AUTO_INCREMENT PRIMARY KEY,
      first_name VARCHAR(32) NOT NULL,
      second_name VARCHAR(32) NOT NULL,
      wemen BOOLEAN NOT NULL
);

CREATE TABLE IF NOT EXISTS home
(   id LONG AUTO_INCREMENT PRIMARY KEY,
    country VARCHAR(50),

    CONSTRAINT fk_user_home
        FOREIGN KEY (id)
            REFERENCES users (id)
);

CREATE TABLE IF NOT EXISTS pet
(
    id LONG AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(32) NOT NULL,
    age INT NOT NULL,
    kind VARCHAR(32) NOT NULL,
    user_id LONG,

    CONSTRAINT fk_user_pet
        FOREIGN KEY (user_id)
            REFERENCES users (id)
);