FROM openjdk:8
COPY target/docker-test.jar docker-test.jar
EXPOSE 8088
ENTRYPOINT ["java", "-jar", "docker-test.jar"]