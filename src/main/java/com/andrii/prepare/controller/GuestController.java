package com.andrii.prepare.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("")
public class GuestController {

    @RequestMapping(method = RequestMethod.GET)
    public String greeting() {
        return "Hello Dear Guest!\nYou can write your name in query(.com?name=Andrii)";
    }
    @RequestMapping(method = RequestMethod.GET, params = {"name"})
    public String greetingWithName(@RequestParam String name) {
        return "Hello, "+name+"!";
    }
}
