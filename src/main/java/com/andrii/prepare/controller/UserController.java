package com.andrii.prepare.controller;

import com.andrii.prepare.dto.UserDto;
import com.andrii.prepare.model.User;
import com.andrii.prepare.service.PetService;
import com.andrii.prepare.service.UserServiceH;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
@Slf4j
public class UserController {

    private final UserServiceH userServiceH;
    private final PetService petService;

    @GetMapping("/{id}")
    public User getUser(@PathVariable long id) {
        return userServiceH.getById(id);
    }

    @GetMapping("")
    public List<User> getAll() {
        return userServiceH.getAll();
    }

    @PostMapping("")
    public User saveUser(@RequestBody UserDto user) {
        return userServiceH.save(user);
    }

    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable long id) {
        userServiceH.delete(id);
    }

    @PutMapping
    public User updateUser(@RequestBody UserDto user) {
        return userServiceH.update(user);
    }

    @PutMapping("/{id}")
    public User updateUserById(@RequestBody UserDto user, @PathVariable int id) {
        return userServiceH.updateById(user, id);
    }
}
