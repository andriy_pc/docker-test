package com.andrii.prepare.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {
    private String firstName;
    private String secondName;
    private boolean wemen;

    public boolean getWemen() {
        return wemen;
    }
}
