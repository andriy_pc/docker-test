package com.andrii.prepare.service;

import com.andrii.prepare.dao.PetDaoH;
import com.andrii.prepare.model.Pet;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class PetService {

    private final PetDaoH daoH;

    public Pet save(Pet pet) {
        daoH.save(pet);
        return pet;
    }

    public Set<Pet> saveAll(Set<Pet> pet) {
        pet.forEach(daoH::save);
        return pet;
    }

    public void delete(long id) {
        Pet petToDelete = daoH.getById(id);
    }


    public List<Pet> getAllByUserId(long userId) {
        return daoH.getByUserId(userId);
    }
}
