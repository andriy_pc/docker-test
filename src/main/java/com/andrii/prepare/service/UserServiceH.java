package com.andrii.prepare.service;

import com.andrii.prepare.dao.UserDaoH;
import com.andrii.prepare.dao.UserDaoJ;
//import com.andrii.prepare.dao.UserDaoM;
import com.andrii.prepare.dto.UserDto;
import com.andrii.prepare.model.User;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserServiceH {
    private final UserDaoH daoH;

    private final UserDaoJ daoJ;


    private final PetService petService;


    private final HomeService homeService;

    public User save(UserDto userDto) {
        User user = mapDtoToModel(userDto);
        User result = daoH.saveUser(user);
        return result;
    }


    public User getById(long id) {
        User result = daoH.getById(id);
        return result;
    }

    public User getBy(long id) {
        return daoJ.findBy(id);
    }

    public void delete(long id) {
        User userToDelete = daoH.getById(id);
        log.info("service contains: {}", daoH.contains(userToDelete));
        daoH.deleteUser(userToDelete);
    }

    public User update(UserDto userDto) {
        User user = mapDtoToModel(userDto);
        return daoJ.save(user);
    }

    public User updateById(UserDto userDto, int id) {
        User user = mapDtoToModel(userDto);
        user.setId(id);
        return daoJ.save(user);
    }

    public List<User> getAll() {
        return daoH.getAll();
    }

    private User mapDtoToModel(UserDto userDto) {
        User result = new User(
                userDto.getFirstName(),
                userDto.getSecondName(),
                userDto.getWemen()
        );
        return result;
    }
}
