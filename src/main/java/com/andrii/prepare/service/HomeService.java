package com.andrii.prepare.service;

import com.andrii.prepare.dao.HomeDaoH;
import com.andrii.prepare.model.Home;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@Data
public class HomeService {
    private final HomeDaoH homeDao;

    public Home save(Home home) {
        return homeDao.save(home);
    }
}
