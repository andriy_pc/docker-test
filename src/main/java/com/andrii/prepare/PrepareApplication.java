package com.andrii.prepare;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories
@SpringBootApplication
@Slf4j
public class PrepareApplication {

    public static void main(String[] args) {
        SpringApplication.run(PrepareApplication.class, args);
    }

}
