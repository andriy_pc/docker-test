package com.andrii.prepare.model;

import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity(name = "user")
@Table(name = "users")
@Data
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
@NamedNativeQuery(name = "User.findBy", query = "SELECT * FROM users WHERE id = ?1", resultClass = User.class)
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "first_name")
    @NonNull
    private String firstName;

    @Column(name = "second_name")
    @NonNull
    private String secondName;

    @Column(name = "wemen")
    @NonNull
    private boolean wemen;

    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private Set<Pet> pets = new HashSet<>();

    @Transient
    @OneToOne
    @JoinColumn(name = "id")
    private Home home;

}
