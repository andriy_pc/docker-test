package com.andrii.prepare.dao;

import com.andrii.prepare.model.Home;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Repository
public class HomeDaoH {

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public Home save(Home home) {
        entityManager.persist(home);
        return home;
    }
}
