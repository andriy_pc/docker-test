package com.andrii.prepare.dao;

import com.andrii.prepare.model.Pet;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class PetDaoH {

    @PersistenceContext
    EntityManager entityManager;

    @Transactional
    public Pet save(Pet pet) {
        entityManager.persist(pet);
        return pet;
    }

    @Transactional
    public void delete(Pet pet) {
        entityManager.remove(pet);
    }

    public Pet getById(long id) {
        return entityManager.find(Pet.class, id);
    }

    public List<Pet> getByUserId(long userId) {
        return entityManager.createQuery("SELECT p FROM pet p WHERE p.user.id =: userId", Pet.class)
        .setParameter("userId", userId)
        .getResultList();
    }
}
