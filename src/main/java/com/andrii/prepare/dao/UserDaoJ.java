package com.andrii.prepare.dao;

import com.andrii.prepare.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.NamedNativeQuery;


@Repository
public interface UserDaoJ extends CrudRepository<User, Long> {

    User findByFirstName(String firstName);

    //    @Query(name="User.findBy", value = "SELECT u FROM user u WHERE u.id = :id")
    User findBy(@Param("id") long id);
}
