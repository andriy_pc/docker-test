package com.andrii.prepare.dao;

import com.andrii.prepare.model.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Transient;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Slf4j
public class UserDaoH {

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public User saveUser(User user) {
        entityManager.persist(user);
        log.info("Dao save: {}", entityManager.contains(user));
        return user;
    }


    @Transactional
    public void deleteUser(User user) {
        entityManager.remove(user);
    }

    @Transactional
    public void deleteUserById(long id) {
        User userToDelete = entityManager.find(User.class, id);
        entityManager.detach(userToDelete);
        entityManager.remove(userToDelete);
    }

    public User getById(long id) {
        return entityManager.find(User.class, id);
    }

    public boolean contains(User user) {
        return entityManager.contains(user);
    }

    @Transactional
    public User update(User user) {
        entityManager.merge(user);
        return user;
    }

    public List<User> getAll() {
        return entityManager.createQuery("SELECT u FROM user u", User.class)
                .getResultList();
    }
}
