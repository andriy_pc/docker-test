package com.andrii.prepare.dao;

import com.andrii.prepare.model.Author;
import com.andrii.prepare.model.Book;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Repository
@Slf4j
public class BookDaoH {
    @PersistenceContext
    EntityManager entityManager;

    @Transactional
    public void save(Book book) {
        entityManager.persist(book);
    }

    @Transactional
    public Book find(int id) {
        Book result = entityManager.find(Book.class, id);
        return result;
    }

    @Transactional
    public void saveAuthor(Author author) {
        entityManager.persist(author);
    }
}
