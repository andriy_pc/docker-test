package com.andrii.prepare.dao;


import com.andrii.prepare.model.Category;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Repository
public class CategoryDaoH {
    @PersistenceContext
    EntityManager entityManager;

    @Transactional
    public void save(Category category) {
        entityManager.persist(category);
    }
}
