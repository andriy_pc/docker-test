package com.andrii.prepare.dao;

import com.andrii.prepare.model.Author;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Repository
public class AuthorDaoH {
    @PersistenceContext
    EntityManager entityManager;

    @Transactional
    public void save(Author author) {
        entityManager.persist(author);
    }

    public Author find(int id) {
        return entityManager.find(Author.class, id);
    }
}
